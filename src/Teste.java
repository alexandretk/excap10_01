public class Teste {

	public static void main(String[] args){
		AreaCalculavel a = new Retangulo(3,2);
		System.out.println("Retangulo " + a.calculaArea());
		
		AreaCalculavel b = new Quadrado(4);
		System.out.println("Quadrado " + b.calculaArea());
		
		AreaCalculavel c = new Circulo(5);
		System.out.println("Circulo " + c.calculaArea());
	}
}
